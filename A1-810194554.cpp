#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace std;

struct Table {
  string name;
  vector<vector<string> > data;
}table;

void replaceComa (string& s) {
  replace( s.begin(), s.end(), ',', ' '); // replace all ',' to ' '
}

void replaceSemi (string& s) {
  replace( s.begin(), s.end(), ';', ' '); // replace all ',' to ' '
}

void replaceParan (string& s) {
  replace( s.begin(), s.end(), '(', ' '); // replace all ',' to ' '
}

void replaceSpace (string& s) {
  replace( s.begin(), s.end(), ' ', '\0'); // replace all ',' to ' '
}

void replaceDouble (string& s) {
  replace( s.begin(), s.end(), '"', ' '); // replace all '"' to '\0'
}

void replaceEqual (string& s) {
  replace( s.begin(), s.end(), '=', ' '); // replace all '=' to ' '
}

void insert (struct Table& temp, vector<string> fields) {
  temp.data.push_back(fields);
}

void create (vector<struct Table>& table,string name, vector<string> fields) {
  struct Table temp;
  temp.name = name; 
  insert(temp, fields);
  table.push_back(temp);
}

void print (struct Table table) {
  if (table.data.empty()) {
    cout << "Empty set";
  } else {
    cout << "Table " << table.name << " has columns [";
    for (int i = 0; i < table.data[0].size() - 1; i++) {
      cout << table.data[0][i] << ", ";
    } 
    cout  << table.data[0][table.data[0].size()-1] << "] and #" << table.data.size() << " rows of data" << endl;
  }
}

void maxSizes (struct Table table, vector<int>& sizes) {
  int temp;
  for (int i = 0; i < table.data[0].size(); i++) {
    temp = 0;
    for (int j = 0 ; j < table.data.size(); j++) {
      if (table.data[j][i].length() > temp) {
        temp = table.data[j][i].length();
      }
    }
    sizes.push_back(temp);
  }
}

void printDivider (vector<int> sizes) {
  cout << "+";
  for (int i = 0; i < sizes.size(); i++) {
    for (int j = 0; j < sizes[i] + 2; j++) {
      cout << "-";
    }
    cout << "+";
  }
  cout << endl;
}

void printSpaces (int j) {
  for (int i = 0; i < j; i++) {
    cout << " ";
  }
}

void printGraphical (struct Table table) {
  vector<int> sizes;
  maxSizes(table, sizes);

  printDivider(sizes);
  for(int i = 0; i < table.data.size(); i++) {
    cout << "| ";
    for (int j = 0; j < table.data[i].size(); j++) {
      cout << table.data[i][j];
      printSpaces(sizes[j] - table.data[i][j].length());
      cout << " | ";
    }
    cout << endl;
    if ( i == 0) {
      printDivider(sizes);
    }
  }
  printDivider(sizes);
}

void select (struct Table table, vector<string> conditions) {

  if (conditions.empty()) {
    if (table.data.size() == 1) {
        cout << "Empty set" << endl;
    } else if (table.data.size() == 2){
        printGraphical(table);
        cout << table.data.size() - 1 << " row in set" << endl;
    } else {
        printGraphical(table);
        cout << table.data.size() - 1 << " rows in set" << endl;
    }
  } else {    
      
    struct Table temp;
    temp.name = "search";
    insert(temp, table.data[0]);
    
    if ((conditions[0].find('"') != string::npos) || (conditions[1].find('"') != string::npos)) {

      if (conditions[0].find('"') != string::npos) {
        conditions[0] = conditions[0].substr(1,conditions[0].length() - 2);
      }

      if (conditions[1].find('"') != string::npos) {
        conditions[1] = conditions[1].substr(1,conditions[1].length() - 2);
      }
        
      for (int j = 0; j < table.data[0].size(); j++) {
        if (table.data[0][j] == conditions[0]) {
          for (int m = 0; m < table.data.size(); m++) {
            if (table.data[m][j] == conditions[1]) {
              insert(temp, table.data[m]);
            }
          }
        } else if (table.data[0][j] == conditions[1]) {
          for (int m = 0; m < table.data.size(); m++) {
            if (table.data[m][j] == conditions[0]) {
              insert(temp, table.data[m]);
            }
          }
        }
      }
      conditions.erase(conditions.begin());
      conditions.erase(conditions.begin());
      select(temp, conditions);

    } else {

      int pos1 = 0, pos2 = 0;
      for (int i = 0; i < table.data[0].size(); i++) {
        if (table.data[0][i] == conditions[0]){
          pos1 = i;
        }
        if (table.data[0][i] == conditions[1]){
          pos2 = i;
        }
      }

      for (int y = 0; y < table.data.size(); y++) {
        if (table.data[y][pos1] == table.data[y][pos2]) {
          insert(temp, table.data[y]);
        }
      }
      conditions.erase(conditions.begin());
      conditions.erase(conditions.begin());
      select(temp, conditions);

    }
    
  }

}

vector<string> createSplit (string s) {
  vector<string> sentenceTokens, final;
  string oprand;
  oprand = s.substr(s.find('(') + 1, s.find(")")- s.find('(') - 1);

  replaceComa(oprand);

  istringstream iss(oprand);   
  copy(istream_iterator<string>(iss),
        istream_iterator<string>(),
        back_inserter(sentenceTokens));

  for (int i = 0; i < sentenceTokens.size(); i+=2) {
    final.push_back(sentenceTokens[i]);
  }

  return final;
}

vector<string> insertSplit (string s) {
  vector<string> sentenceTokens;
  string oprand;
  oprand = s.substr(s.find('(') + 1, s.find(')')- s.find('(') - 1);

  replaceComa(oprand);

  while (oprand.find('"') != string::npos) {
    oprand = oprand.substr(oprand.find('"') + 1);
    sentenceTokens.push_back(oprand.substr(0 , oprand.find('"')));
    oprand = oprand.substr(oprand.find('"') + 1);
  }

  return sentenceTokens;
}


void split_string(string text,vector<string>& words)
{
  char ch;

  while(ch=text[0]) {
    if (!isspace(ch) && (ch != '"')) {
      words.push_back(text.substr(0 , text.find(' ')));
      text = text.substr(text.find(' ') + 1);
    } else if (ch == '"') {
      text = text.substr(text.find('"') + 1);
      words.push_back('"' + text.substr(0 , text.find('"') + 1));
      text = text.substr(text.find('"') + 1);
    } else {
      text = text.substr(1);
    }
  }
}

vector<string> selectSplit (string s) {
  vector<string> final;
  
  s = s + " ";
  
  replaceComa(s);
  replaceEqual(s);

  int flag = 0;
  for (int i = 0; i < s.length()-5; i++) {
    if (s[i] == 'W' && s[i+1] == 'H' && s[i+2] == 'E' && s[i+3] == 'R' && s[i+4] == 'E') {
      s = s.substr(i+5);
      flag = 1;
      break;
    }
  }

  if (flag == 1) {
    split_string(s, final);
  }
  
  return final;
}


int findTable (vector<struct Table> table, string name) {
  for (int i = 0; i < table.size(); i++) {
    if (table[i].name == name) {
      return i;
    }
  }
  return -1;
}

int main() {

  struct Table temp;
  vector<struct Table> table;

  string sentence, s;
  vector<string> sentenceTokens;
  vector<string> fields;


  while (1) {
    getline(cin, sentence);

    if ( sentence == "" ) {
	continue;
    }
    
    if (sentence == "Exit") {
      return 0;
    } else {
      sentenceTokens.clear();

      sentence = sentence.substr(0, sentence.size() - 1);
      s = sentence;

      replaceSemi(s);
        
      replaceParan(s);

      istringstream iss(s);
        
      copy(istream_iterator<string>(iss),
           istream_iterator<string>(),
           back_inserter(sentenceTokens));
      if (sentenceTokens[0] == "CREATE") {
        create(table, sentenceTokens[2], createSplit(sentence));
        cout << "Query OK" << endl;
      } else if (sentenceTokens[0] == "INSERT") {
        int tableNum = findTable(table, sentenceTokens[2]);
        if (tableNum == -1) {
          cout << "There is no table with name " << sentenceTokens[2] << " !!!" << endl;
        } else {
          insert(table[tableNum], insertSplit(sentence));
          cout << "Query OK" << endl;
        }
        //printGraphical(table[tableNum]);
      } else if (sentenceTokens[0] == "SELECT") {
        int tableNum = findTable(table, sentenceTokens[3]);
        if (tableNum == -1) {
          cout << "There is no table with name " << sentenceTokens[3] << " !!!" << endl;
        } else {
          select(table[tableNum], selectSplit(sentence));
        }
      } else {
        cout << "Invalid Request" << endl;
      }
    }
  }
  
  return 0;
}
